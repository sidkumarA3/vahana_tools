#!/usr/bin/env python3
# encoding: utf-8

import sys
import os
import datetime
import sqlite3 as sqlite
import re

LINE_NUMBER = 1


def print_to_file(string, flush=True):
    global LINE_NUMBER
    print(string, flush=flush)
    with open(sys.argv[3], 'a+') as file:
        string = ",".join(re.split(' +', string))
        file.write("{},{}\n".format(LINE_NUMBER, string))
        LINE_NUMBER += 1


def get_timestamps(file, data):
    try:
        db_connection = sqlite.connect(file)
        db_cursor = db_connection.cursor()
        db_cursor.execute('SELECT name FROM sqlite_master WHERE type=\'table\'')
        table_names = db_cursor.fetchall()
        for table in table_names:
            if table[0] == 'RTILog':
                db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("Timestamp", table[0]))
            else:
                db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("SampleInfo_source_timestamp", table[0]))
            ts = db_cursor.fetchall()
            skip = False
            if "deserialized" in file.split('/')[-1]:
                if "RadioTelemetry" in table[0]:
                    db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("location", table[0]))
                    location = db_cursor.fetchall()
                    if (0,) in location:
                        data[table[0].split('$')[0] + '0'] = int(ts[location.index((0,))][0])
                    if (1,) in location:
                        data[table[0].split('$')[0] + '1'] = int(ts[location.index((1,))][0])
                    skip = True
                if "Battery" in table[0]:
                    db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("instance", table[0]))
                    instance = db_cursor.fetchall()
                    for i in range(4):
                        if (i,) in instance:
                            data[table[0].split('$')[0] + str(i)] = int(ts[instance.index((i,))][0])
                    skip = True
            try:
                if not skip:
                    data[table[0].split('$')[0]] = int(ts[0][0])
            except IndexError:
                pass
            except ValueError:
                pass
    except sqlite.DatabaseError as e:
        print("ERROR: parsing database " + file)
    return data

def generate_report(files):
    mc_dict = dict()
    gcs_dict = dict()
    for file in files:
        if "mc_flight_" in file:
            mc_dict = get_timestamps(file, mc_dict)
        elif "gcs_flight_" in file:
            gcs_dict = get_timestamps(file, gcs_dict)
    print("Topic" + "                        "  + "Diff(s)" + "  " + "%Y-%m-%d %H:%M:%S", flush=True)
    LETTERS = 35
    for k in mc_dict.keys():
        if k in gcs_dict.keys():
            spaces = ""
            for i in range(LETTERS - len(k)):
                spaces = spaces + " "
            td = float(mc_dict[k] - gcs_dict[k])/pow(10, 9)
            if td == 0.0:
                print_to_file(k + spaces + str(td) + " " + datetime.datetime.utcfromtimestamp(gcs_dict[k]/pow(10, 9)).strftime('%Y-%m-%d %H:%M:%S.%f') + "  " + str(mc_dict[k]) + "   " + str(gcs_dict[k]), flush=True)
            else:
                print_to_file(k + spaces + str(td) + "  " + datetime.datetime.utcfromtimestamp((gcs_dict[k]/pow(10, 9))).strftime('%Y-%m-%d %H:%M:%S.%f'), flush=True )
        else:
            print_to_file(k)
    if "RadioTelemetry0" in gcs_dict.keys() and "RadioTelemetry1" in mc_dict.keys():
        spaces = ""
        for i in range(LETTERS - len("DiffRadioTelemetry")):
            spaces = spaces + " "
        td = float(mc_dict["RadioTelemetry1"] - gcs_dict["RadioTelemetry0"]) / pow(10, 9)
        print_to_file("DiffRadioTelemetry" + spaces + str(td) + "  " + datetime.datetime.utcfromtimestamp(
            (gcs_dict["RadioTelemetry0"] / pow(10, 9))).strftime('%Y-%m-%d %H:%M:%S.%f'), flush=True)
    print("***********************************", flush=True)
    print("Telemetry only Topics", flush=True)
    print("Topic" + "                        " + "Start Time", flush=True)
    for key in gcs_dict.keys():
        if key not in mc_dict.keys():
            spaces = ""
            for i in range(LETTERS - len(key)):
                spaces = spaces + " "
            print_to_file(key + spaces + "  " + datetime.datetime.utcfromtimestamp((gcs_dict[key] / pow(10, 9))).strftime(
                '%Y-%m-%d %H:%M:%S'), flush=True)


def create_timestamped_database_list(path_base, type):
    ret = list()
    try:
        for f in os.listdir(path_base):
            if os.path.isfile(os.path.join(path_base, f)) and type in f:
                path = os.path.join(path_base, f)
                filename_parts = f.split('_')
                datetime_format = "%Y-%m-%d%H%M%S"
                try:
                    timestamp = datetime.datetime.strptime(filename_parts[3] + filename_parts[4], datetime_format)
                    ret.append((path, timestamp))
                except ValueError as e:
                    print("ERROR: Parsing Timestamp for " + f + " " + str(e))
    except FileNotFoundError:
        print("WARNING: Path Not Found {}".format(path_base))
    return ret


def crawler(root_dir, id):
    vehicle = 'alpha'
    bundle = 'unknown'
    for state in ['staging', 'archive']:
        curr_dir = os.path.join(root_dir, vehicle, state)
        dirs = os.listdir(curr_dir)
        dirs.sort(key=lambda x: int(re.search(r'(?<=ID)[0-9]*', x).group()), reverse=True)
        for dir in dirs:
            if int(re.search(r'(?<=ID)[0-9]*', dir).group()) > int(id):
                continue
            with open(os.path.join(curr_dir, dir, 'onboard_logs', 'versions')) as file:
                versions = file.readlines()
                for line in versions:
                    if 'BUNDLE' in line:
                        bundle = line.split('=')[1]
                        break
            onboard_logs = create_timestamped_database_list(os.path.join(curr_dir, dir, 'onboard_logs'),
                                                            'mc_flight_data')
            telemetry_logs = create_timestamped_database_list(os.path.join(curr_dir, dir, 'telemetry_logs'),
                                                            'gcs_flight_data')
            print_to_file("BUNDLE {}".format(bundle), flush=True)
            print_to_file("TEST {}".format(dir), flush=True)
            if len(telemetry_logs) == 0 or len(onboard_logs) == 0:
                print("WARNING: No Telemetry or Onboard Logs!! Skipping")
                continue
            for mc in onboard_logs:
                min_timedelta = datetime.timedelta(seconds=10)
                if 'deserialized' in mc[0]:
                    list_of_dbs = [i[0] for i in onboard_logs if abs(i[1] - mc[1]) <= min_timedelta]
                    temp_list = ([i[0] for i in telemetry_logs if abs(i[1] - mc[1]) <= min_timedelta])
                    if len(temp_list) == 0:
                        continue
                    list_of_dbs.extend(temp_list)
                    print_to_file("Record  {}".format(mc[0].split('/')[-1]), flush=True)
                    generate_report(list_of_dbs)


crawler(sys.argv[1], sys.argv[2])