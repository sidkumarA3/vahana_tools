#!/usr/bin/env python3
# encoding: utf-8

import sys
import traceback
import re
import matplotlib.pyplot as plt
import matplotlib
import csv
import datetime

LIST_LABELS = ['DiffRadioTelemetry',
               'FlightControlTelemetry',
               'ActuatorsProxyHealth',
               'ActuatorsTelemetry',
               'AglTelemetry',
               'SensorsProxyHealth',
               'CurrentTrajectory',
               'RTILog']


def read_csv(file):
    global LIST_LABELS
    with open(file) as f:
        parsed_csv = csv.reader(f, delimiter=",")
        bundle = 0
        test = 0
        record = ""

        class data_point:
            def __init__(self):
                self.test = list()
                self.bundle = list()
                self.line_no = list()
                self.dt = list()
                self.record = list()
                self.date = list()

        plots = dict()
        for line in parsed_csv:
            try:
                if 'BUNDLE' in line[1]:
                    bundle = int(line[2])
                elif 'TEST' in line[1]:
                    test = int(re.search(r'(?<=ID)[0-9]*', line[2]).group())
                elif 'Record' == line[1]:
                    record = line[2]
                else:
                    if line[1] in LIST_LABELS:
                        data = float(line[2])   # Check if Diff value is valid
                        if line[1] not in plots.keys():
                            plots[line[1]] = data_point()
                        plots[line[1]].dt.append(data)
                        plots[line[1]].line_no.append(int(line[0]))
                        plots[line[1]].bundle.append(bundle)
                        plots[line[1]].test.append(test)
                        plots[line[1]].record.append(record)
                        record_parts = record.split('_')
                        datetime_format = "%Y-%m-%d%H%M%S"
                        try:
                            timestamp = datetime.datetime.strptime(record_parts[3] + record_parts[4],
                                                                   datetime_format)
                        except ValueError as e:
                            print("ERROR: Parsing Timestamp for " + f + " " + str(e))
                            timestamp = datetime.datetime.timestamp()
                        plots[line[1]].date.append(timestamp)

            except ValueError as e:
                print("Value Error as " + str(e))
                print(traceback.format_exc())
                print(line)
            except IndexError as e:
                pass

    fig_list = [plt.figure(i) for i in range(4)]

    plt_list = [fig_list[i].add_subplot(111) for i in range(4)]

    for label in plots.keys():
        plt_list[0].scatter(plots[label].bundle, plots[label].dt, marker='o', label=label)

        plt_list[1].scatter(plots[label].test, plots[label].dt, marker='o', label=label)

        plt_list[2].scatter(plots[label].line_no, plots[label].dt, marker='o', label=label)

        dates = matplotlib.dates.date2num(plots[label].date)
        plt_list[3].plot_date(dates, plots[label].dt, marker='o', label=label)

    plt_list[0].legend(loc='best')
    plt_list[0].set_xlabel("Bundle Number")
    plt_list[0].set_ylabel("Diff Time (ms)")
    plt_list[0].grid('on')

    plt_list[1].legend(loc='best')
    plt_list[1].set_xlabel("Test ID Number")
    plt_list[1].set_ylabel("Diff Time (ms)")
    plt_list[1].grid('on')

    plt_list[2].legend(loc='best')
    plt_list[2].set_xlabel("Line Number")
    plt_list[2].set_ylabel("Diff Time (ms)")
    plt_list[2].grid('on')

    plt_list[3].legend(loc='best')
    plt_list[3].set_xlabel("Record Date")
    plt_list[3].set_ylabel("Diff Time (ms)")
    plt_list[3].grid('on')

    plt.show()

if __name__ == '__main__':
    read_csv(sys.argv[1])