#!/usr/bin/env python3
# encoding: utf-8

import sys
import os
import datetime
import sqlite3 as sqlite
import time
import re

# Configure system path so that we can access to the Vahana module
sys.path.insert(0, '/home/vahana/vahana_test/package')

# Instantiate a Vahana object
from vah import vahana
from vah.deserialize import deserialize

vahana = vahana.Vahana()


recording_systems = ['MC','GCS']
LINE_NUMBER = 1


def print_to_file(string, flush=True):
    global LINE_NUMBER
    print(string, flush=flush)
    with open(sys.argv[4], 'a+') as file:
        string = ",".join(re.split(' +', string))
        file.write("{},{}\n".format(LINE_NUMBER, string))
        LINE_NUMBER += 1


def save_records():
    vahana.recorder.stop(True, recording_systems)
    date = datetime.datetime.now().strftime("%B-%d-%Y-%H-%M-%S")
    dir = "records_" + date
    output_directory = sys.argv[1] + dir
    os.mkdir(output_directory)

    mc_record_remote_path = vahana.vehicle.mission_computer.get_last_record_path()
    gcs_record_remote_path = vahana.gcs.test_automation.get_last_record_path()
    gcs_recorder_log_remote_path = vahana.gcs.test_automation.get_last_recorder_log_path()

    mc_record_local_path = os.path.join(output_directory, 'mc_flight_data_' + date)
    vahana.vehicle.mission_computer.download(mc_record_remote_path, mc_record_local_path, delete_src= True)
    mc_record = deserialize(mc_record_local_path)

    gcs_record_local_path = os.path.join(output_directory, 'gcs_flight_data_' + date)
    gcs_record = vahana.gcs.test_automation.download(gcs_record_remote_path, gcs_record_local_path, delete_src=True)
    gcs_record = deserialize(gcs_record_local_path)

    gcs_recorder_log_local_path = os.path.join(output_directory, 'gcs_recorder_log_' + date)
    gcs_recorder_log = vahana.gcs.test_automation.download(gcs_recorder_log_remote_path, gcs_recorder_log_local_path, delete_src=True)

    return output_directory


def get_timestamps(file, data):
    try:
        db_connection = sqlite.connect(file)
        db_cursor = db_connection.cursor()
        db_cursor.execute('SELECT name FROM sqlite_master WHERE type=\'table\'')
        table_names = db_cursor.fetchall()
        for table in table_names:
            if table[0] == 'RTILog':
                db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("Timestamp", table[0]))
            else:
                db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("SampleInfo_source_timestamp", table[0]))
            ts = db_cursor.fetchall()
            skip = False
            if "deserialized" in file.split('/')[-1]:
                if "RadioTelemetry" in table[0]:
                    db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("location", table[0]))
                    location = db_cursor.fetchall()
                    if (0,) in location:
                        data[table[0].split('$')[0] + '0'] = int(ts[location.index((0,))][0])
                    if (1,) in location:
                        data[table[0].split('$')[0] + '1'] = int(ts[location.index((1,))][0])
                    skip = True
                if "Battery" in table[0]:
                    db_cursor.execute('SELECT \"{}\" FROM \"{}\"'.format("instance", table[0]))
                    instance = db_cursor.fetchall()
                    for i in range(4):
                        if (i,) in instance:
                            data[table[0].split('$')[0] + str(i)] = int(ts[instance.index((i,))][0])
                    skip = True
            try:
                if not skip:
                    data[table[0].split('$')[0]] = int(ts[0][0])
            except IndexError:
                pass
            except ValueError:
                pass
    except sqlite.DatabaseError as e:
        print("ERROR: parsing database " + file)
    return data


def generate_report(dire):
    mc_dict = dict()
    gcs_dict = dict()
    for file in os.listdir(dire):
        if "mc_flight_" in file:
            mc_dict = get_timestamps(file, mc_dict)
        elif "gcs_flight_" in file:
            gcs_dict = get_timestamps(file, gcs_dict)
    print("Topic" + "                        "  + "Diff(s)" + "  " + "%Y-%m-%d %H:%M:%S", flush=True)
    LETTERS = 35
    for k in mc_dict.keys():
        if k in gcs_dict.keys():
            spaces = ""
            for i in range(LETTERS - len(k)):
                spaces = spaces + " "
            td = float(mc_dict[k] - gcs_dict[k])/pow(10, 9)
            if td == 0.0:
                print_to_file(k + spaces + str(td) + " " + datetime.datetime.utcfromtimestamp(gcs_dict[k]/pow(10, 9)).strftime('%Y-%m-%d %H:%M:%S.%f') + "  " + str(mc_dict[k]) + "   " + str(gcs_dict[k]), flush=True)
            else:
                print_to_file(k + spaces + str(td) + "  " + datetime.datetime.utcfromtimestamp((gcs_dict[k]/pow(10, 9))).strftime('%Y-%m-%d %H:%M:%S.%f'), flush=True )
        else:
            print_to_file(k)
    if "RadioTelemetry0" in gcs_dict.keys() and "RadioTelemetry1" in mc_dict.keys():
        spaces = ""
        for i in range(LETTERS - len("DiffRadioTelemetry")):
            spaces = spaces + " "
        td = float(mc_dict["RadioTelemetry1"] - gcs_dict["RadioTelemetry0"]) / pow(10, 9)
        print_to_file("DiffRadioTelemetry" + spaces + str(td) + "  " + datetime.datetime.utcfromtimestamp(
            (gcs_dict["RadioTelemetry0"] / pow(10, 9))).strftime('%Y-%m-%d %H:%M:%S.%f'), flush=True)
    print("***********************************", flush=True)
    print("Telemetry only Topics", flush=True)
    print("Topic" + "                        " + "Start Time", flush=True)
    for key in gcs_dict.keys():
        if key not in mc_dict.keys():
            spaces = ""
            for i in range(LETTERS - len(key)):
                spaces = spaces + " "
            print_to_file(key + spaces + "  " + datetime.datetime.utcfromtimestamp((gcs_dict[key] / pow(10, 9))).strftime(
                '%Y-%m-%d %H:%M:%S'), flush=True)


if __name__ == '__main__':
    vahana.recorder.stop(True, recording_systems)
    for i in range(int(sys.argv[2])):
        print_to_file("TEST      ID{}".format(i), flush=True)
        vahana.recorder.erase()
        time.sleep(10)
        vahana.recorder.start(True, recording_systems)
        time.sleep(int(sys.argv[3]))
        generate_report(save_records())
