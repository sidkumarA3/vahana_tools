#!/usr/bin/env python3

import progressbar
import time
import random
from multiprocessing import Pool


def random_wait(i):
    time.sleep(random.randrange(i))


def main():
    print("Hello")
    pool = Pool(processes=4, maxtasksperchild=1)

    progress_bar = progressbar.ProgressBar().start(max_value=20)
    prev_count = 0
    thread_results = pool.map_async(random_wait, range(0, 20))
    while not thread_results.ready():
        count = 20 - thread_results._number_left
        if prev_count < count:
            progress_bar.update(count, force=True)
            prev_count = count
        time.sleep(0.1)
    progress_bar.finish()


if __name__ == '__main__':
    main()
