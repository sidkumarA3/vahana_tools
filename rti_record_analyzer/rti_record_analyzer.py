#!/usr/bin/env python3
"""Summary
"""

import pandas
import datetime
import progressbar
import geopy.distance
import rti_record_parser

def sanatize_db(db):
    """Summary
    
    Args:
        db (TYPE): Description
    
    Returns:
        TYPE: Description
    """
    db = db.split('/')[-1]
    db = '_'.join(db.split('.'))
    db = '_'.join(db.split('-'))
    return db

class PandaSeriesOperations():

    """Summary
    
    Attributes:
        db (TYPE): Description
        parser (TYPE): Description
        series (TYPE): Description
        type_tree (TYPE): Description
    """
    
    def __init__(self, type_tree, parser, db):
        """Summary
        
        Args:
            type_tree (TYPE): Description
            parser (TYPE): Description
            db (TYPE): Description
        """
        self.parser = parser
        self.series = dict()
        self.db = db
        self.type_tree = type_tree

    def SubtractTimeSeries(self, series1, series2):
        """Summary
        
        Args:
            series1 (TYPE): Description
            series2 (TYPE): Description
        
        Returns:
            TYPE: Description
        """
        index_zip = zip()
        index = list()
        data = list()
        name = series1.name + ' - ' + series2.name
        if (series1.index[0] < series2.index[0]):
            series1 = series1[series1.index.indexer_between_time(series2.index[0].to_pydatetime().time(),
                                                                 series2.index[-1].to_pydatetime().time())]
        elif (series1.index[0] > series2.index[0]):
            series2 = series2[series2.index.indexer_between_time(series1.index[0].to_pydatetime().time(),
                                                                 series1.index[-1].to_pydatetime().time())]
            series1 = series1[1:]
        if (len(series1.index) > len(series2.index)):
            index_zip = zip(series1.index[:(len(series2.index) -
                                           len(series1.index))], series2.index)
        elif (len(series1.index) < len(series2.index)):
            index_zip = zip(series1.index, series2.index[:(len(series1.index) -
                                                          len(series2.index))])
        else:
            index_zip = zip(series1.index, series2.index)
        for (i1, i2) in index_zip:
            try:
                data.append(series1[i1] - series2[i2])
                index.append(i1)
            except KeyError:
                continue
        return(pandas.Series(
            data=data,
            index=index,
            name=name))

    def TimeSeries(self, type, unix_time=False, source_time=False, db_name=None):
        """Summary
        
        Args:
            type (TYPE): Description
            unix_time (bool, optional): Description
            source_time (bool, optional): Description
            db_name (None, optional): Description
        
        Returns:
            TYPE: Description
        """
        if db_name:
            if not isinstance(db_name, list):
                db = sanatize_db(db_name)
                temp_type_tree = vars(self.type_tree)[db]
                temp_parser = vars(self.parser)[db]
                if type.topic in vars(temp_type_tree):
                    if type.name in vars(temp_type_tree)[type.topic].types():
                        temp_series = self.GenerateTimeSeries(temp_parser, type, unix_time, source_time)
                        return temp_series
            else:
                series = None
                print("Topic: " + type.topic + "/" + type.name)
                with progressbar.ProgressBar(max_value=len(db_name)) as progress:
                    i = 0
                    for db in db_name:
                        db = sanatize_db(db)
                        temp_type_tree = vars(self.type_tree)[db]
                        temp_parser = vars(self.parser)[db]
                        if type.topic in vars(temp_type_tree):
                            if type.name in vars(temp_type_tree)[type.topic].types():
                                temp_series = self.GenerateTimeSeries(temp_parser, type, unix_time, source_time)
                                if series is not None:
                                    series = series.append(temp_series).sort_index()
                                    series.name = temp_series.name
                                else:
                                    series = temp_series
                            else:
                                print("Invalid Topic Variable " + type.name)
                        i = i + 1
                        progress.update(i)
                return series
        else:
            if type.topic in self.series.keys():
                if type.name in self.series[type.topic].keys():
                    return self.series[type.topic][type.name]
            print("Topic: " + type.topic + "/" + type.name)
            with progressbar.ProgressBar(max_value = len(self.db)) as progress:
                i = 0
                for db in self.db:
                    db = sanatize_db(db)
                    temp_type_tree = vars(self.type_tree)[db]
                    temp_parser = vars(self.parser)[db]
                    if type.topic in vars(temp_type_tree):
                        if type.topic not in self.series.keys():
                            self.series[type.topic] = dict()
                        if type.name in vars(temp_type_tree)[type.topic].types():
                            temp_series = self.GenerateTimeSeries(temp_parser, type, unix_time, source_time)
                            if type.name in self.series[type.topic].keys():
                                self.series[type.topic][type.name] = self.series[type.topic][type.name].append(temp_series).sort_index()
                                self.series[type.topic][type.name].name = temp_series.name
                            else:
                                self.series[type.topic][type.name] = temp_series
                        else:
                            print("Invalid Topic Variable " + type.name)
                    i = i + 1
                    progress.update(i)
            return  self.series[type.topic][type.name]


    def GetDistance(self, loc, lat2, lon2):
        dist = geopy.distance.vincenty((loc['VehicleState_1/latitude_rad'], loc['VehicleState_1/longitude_rad']),
                                       (lat2,lon2)).m
        return dist

    def GenerateDistanceSeries(self, location, db_name=None):
        lat_type = rti_record_parser.RtiRecordParser.Type('latitude_rad', 'VehicleState_1')
        lon_type = rti_record_parser.RtiRecordParser.Type('longitude_rad', 'VehicleState_1')
        lat_series = self.TimeSeries(lat_type, db_name=db_name).multiply(57.2958)
        lon_series = self.TimeSeries(lon_type, db_name=db_name).multiply(57.2958)
        loc_series = pandas.concat([lat_series, lon_series], axis=1)
        return loc_series.apply(self.GetDistance, args=(location[0], location[1],), axis=1)

    def GenerateClosestToLocationSeries(self, series, location, db_name=None):
        print("Finding {} closest to {},{}".format(series.name,location[0],location[1]))
        dist_series = self.GenerateDistanceSeries(series,location, db_name)
        dist_series = dist_series.loc[dist_series < 20.0]
        return self.GenerateTimeFittingSeries(dist_series, series)

    def GenerateTimeFittingSeries(self,series1, series2):
        start_index = self.GetClosestIndex(series1.index[0], series2)
        end_index = self.GetClosestIndex(series1.index[-1], series2)
        return series2[start_index:end_index]

    def GenerateClosestFittingSeries(self, series1, series2, time=True):
        data = list()
        with progressbar.ProgressBar(max_value=len(series1.index)) as progress:
            k = 0
            for i in series1.index:
                data.append(series2[self.GetClosestIndex(i, series2, time)])
                k = k + 1
                progress.update(k)
        return pandas.Series(data=data, index=series1.index)

    def GetClosestIndex(self, index, series, time=True):
        if time:
            index =  pandas.to_datetime(index)
        return series.index.get_loc(index, method='nearest')

    def GenerateTimeSeries(self, parser, type, unix_time=False, source_time=False):
        """Summary
        
        Args:
            parser (TYPE): Description
            type (TYPE): Description
            unix_time (bool, optional): Description
            source_time (bool, optional): Description
        
        Returns:
            TYPE: Description
        """

        if not source_time:
            time = parser.GetDataSet(type.topic,
                                          'SampleInfo_reception_timestamp')
        else:
            time = parser.GetDataSet(type.topic,
                                          'SampleInfo_source_timestamp')
        if not unix_time:
            time = [datetime.datetime.utcfromtimestamp((timestamp / (10e8)))
                    for timestamp in time]
        _data = parser.GetDataSet(type)
        if _data.size != 0:
            if _data.size < len(time):
                for i in range(len(time) - _data.size):
                    time.pop()
            series = pandas.Series(
                data=_data,
                index=time)
        else:
            series = pandas.Series(
                data=[],
                index=[])
        series.name=(type.topic + "/" + type.name)
        return series


class RtiRecordAnalyzer():

    """Summary
    
    Attributes:
        event_handler (TYPE): Description
        series (TYPE): Description
        series_queue (TYPE): Description
        source_time (bool): Description
        unix_time (bool): Description
    """
    
    def __init__(self, series, event_handler, series_queue, path="."):
        """Summary
        
        Args:
            series (TYPE): Description
            event_handler (TYPE): Description
            series_queue (TYPE): Description
        """
        self.path = path
        self.series = series
        self.event_handler = event_handler
        self.series_queue = series_queue
        self.unix_time = False
        self.source_time = True

    def PlotData(self, type, rad=True, db = None):
        """Summary
        
        Args:
            type (TYPE): Description
            rad (bool, optional): Description
            db (string, optional): Description
        """
        series = self.series.TimeSeries(type, self.unix_time, self.source_time, db)
        if not rad:
            if type.name[-3:] == 'rad' or type.name[-4:-1] == 'rad':
                series = series.multiply(57.2958)
        self.series_queue.put(series)
        self.event_handler.SetDraw()

    def PlotScaledData(self, type, scalefactor, mul = False, db = None):
        """Summary
        
        Args:
            type (TYPE): Description
            scalefactor (float): Description
            mul (bool, optional): Description
            db (string, optional): Description
        """
        scalefactor = float(scalefactor)
        series = self.series.TimeSeries(type, self.unix_time, self.source_time, db)
        if mul:
            series = series.multiply(scalefactor)
            series.name = (series.name + '*' + str(scalefactor))
        else:
            series = series.multiply((1.0/scalefactor))
            series.name = (series.name + '/' + str(scalefactor))
        print(series.name)
        self.series_queue.put(series)
        self.event_handler.SetDraw()

    def GetCsv(self, types, file, rad=True, hex_flag=False, bin_flag=False, db=None, resample=None):
        """Summary
        
        Args:
            types (TYPE): Description
            file (string): Description
            rad (bool, optional): Description
            hex_flag (bool, optional): Description
            bin_flag (bool, optional): Description
            db (string, optional): Description
        """
        def convert_to_hex(x):
            return hex(int(x))

        def convert_to_bin(x):
            return bin(int(x))

        series = list()
        try:
            same_topic = True
            for i in range(0, len(types)-1):
                if types[i].topic != types[i+1].topic:
                    same_topic = False
                    break
            if same_topic:
                for d_type in types:
                    temp = self.series.TimeSeries(d_type, self.unix_time, self.source_time, db)
                    if not rad:
                        if d_type.name[-3:] == 'rad' or d_type.name[-4:-1] == 'rad':
                            temp = temp.multiply(57.2958)
                    if hex_flag:
                        temp = temp.apply(convert_to_hex)
                    elif bin_flag:
                        temp = temp.apply(convert_to_bin)
                    if resample is not None:
                        temp = temp.resample(resample).mean()
                    series.append(temp)
            else:
                print("Topics are different using the first topic to create a best fit time series")
                first_topic = self.series.TimeSeries(types[0], self.unix_time, self.source_time, db)
                series.append(first_topic)
                for d_type in types[1:]:
                    print("Topic: " + d_type.topic + "/" +  d_type.name)
                    temp = self.series.TimeSeries(d_type, self.unix_time, self.source_time, db)
                    if not rad:
                        if d_type.name[-3:] == 'rad' or d_type.name[-4:-1] == 'rad':
                            temp = temp.multiply(57.2958)
                    if hex_flag:
                        temp = temp.apply(convert_to_hex)
                    elif bin_flag:
                        temp = temp.apply(convert_to_bin)
                    series.append(self.series.GenerateClosestFittingSeries(first_topic,temp))
            series = pandas.concat(series, axis=1)
        except TypeError:
            series = self.series.TimeSeries(types, self.unix_time, self.source_time, db)
            if not rad:
                if types.name[-3:] == 'rad' or types.name[-4:-1] == 'rad':
                    series = series.multiply(57.2958)
            if hex_flag:
                series = series.apply(convert_to_hex)
            elif bin_flag:
                series = series.apply(convert_to_bin)
        series.to_csv(self.path + "/" + file)
        print("Saved: " + self.path + "/" + file)

    def PlotDtData(self, type, rad=True, db = None):
        """Summary
        
        Args:
            type (TYPE): Description
            rad (bool, optional): Description
            db (None, optional): Description
        """
        series = self.series.TimeSeries(type, self.unix_time, self.source_time, db)
        if not rad:
            if type.name[-3:] == 'rad' or type.name[-4:-1] == 'rad':
                series = series.multiply(57.2958)
        series2 = series.iloc[:-1]
        series1 = series.iloc[1:]
        diffseries = self.series.SubtractTimeSeries(series1, series2)
        self.series_queue.put(diffseries)
        self.event_handler.SetDraw()

    def PlotDiffData(self, type1, type2, rad=False, db=None):
        series1 = self.series.TimeSeries(type1, self.unix_time, self.source_time, db)
        series2 = self.series.TimeSeries(type2, self.unix_time, self.source_time, db)
        diffseries = self.series.SubtractTimeSeries(series1, series2)
        if not rad:
            if type1.name[-3:] == 'rad' or type1.name[-4:-1] == 'rad':
                diffseries = diffseries.multiply(57.2958)
        self.series_queue.put(diffseries)
        self.event_handler.SetDraw()

    def PlotDataVsHeading(self, type1, rad=False, db =None, location=None):
        if location:
            series1 = self.GenClosestSeries(type1, location, db)
        if not rad:
            if type1.name[-3:] == 'rad' or type1.name[-4:-1] == 'rad':
                series1 = series1.multiply(57.2958)
        type2 = rti_record_parser.RtiRecordParser.Type('heading_rad', 'VehicleState_1')
        if location:
            series2 = self.GenClosestSeries(type2, location, db)
        else:
            series2 = self.series.TimeSeries(type2, self.unix_time, self.source_time, db)
        series2 = self.series.GenerateClosestFittingSeries(series1, series2)
        versus_series = pandas.Series(data=list(series1.data), index=list(series2.data))
        versus_series.name = type1.name + "Vs" + type2.name
        self.series_queue.put(versus_series)
        self.event_handler.SetDraw()
        return versus_series

    def PlotDataVsDistance(self, type1, location, rad=False, db=None):
        series1 = self.series.TimeSeries(type1, self.unix_time, self.source_time, db)
        if not rad:
            if type1.name[-3:] == 'rad' or type1.name[-4:-1] == 'rad':
                series1 = series1.multiply(57.2958)
        dist_series = self.series.GenerateDistanceSeries(location, db)
        series2 = self.series.GenerateClosestFittingSeries(series1, dist_series)
        versus_series = pandas.Series(data=list(series1.data), index=list(series2.data))
        versus_series.name = type1.name + "VsDistance_m"
        self.series_queue.put(versus_series)
        self.event_handler.SetDraw()
        return versus_series




    def PlotVsData(self, type1, type2, rad=False, db =None, location=None):
        """Summary
        
        Args:
            type1 (TYPE): Description
            type2 (TYPE): Description
            rad (bool, optional): Description
            db (string, optional): Description
        """
        if location:
            series1 = self.GenClosestSeries(type1, location, db)
        else:
            series1 = self.series.TimeSeries(type1, self.unix_time, self.source_time, db)
        if not rad:
            if type1.name[-3:] == 'rad' or  type1.name[-4:-1] == 'rad':
                series1 = series1.multiply(57.2958)
        if location:
            series2 = self.GenClosestSeries(type2, location, db)
        else:
            series2 = self.series.TimeSeries(type2, self.unix_time, self.source_time, db)
        if not rad:
            if type1.name[-3:] == 'rad' or type1.name[-4:-1] == 'rad':
                series2 = series2.multiply(57.2958)
        if type1.topic != type2.topic:
            series2 = self.series.GenerateClosestFittingSeries(series1, series2)
        versus_series = pandas.Series(data=list(series1.data), index=list(series2.data))
        versus_series.name = type1.name + "Vs" + type2.name
        self.series_queue.put(versus_series)
        self.event_handler.SetDraw()
        return versus_series

    def GenClosestSeries(self, type, location, db=None):
        series = self.series.TimeSeries(type, self.unix_time, self.source_time, db)
        return self.series.GenerateClosestToLocationSeries(series,location,db_name=db)


    def SaveFigure(self, file_name):
        """Summary
        
        Args:
            file_name (string): Description
        """
        self.series_queue.put(file_name)
        self.event_handler.SetSave()

    def ClearFigure(self):
        """Summary
        """
        self.event_handler.SetClear()

    def AddSubPlot(self):
        """Summary
        """
        self.event_handler.SetAdd()

    def CreateNewPlot(self):
        """Summary
        """
        self.event_handler.SetNew()
