#!/usr/bin/env python3
"""Summary
"""

import sys
import sqlite3 as sqlite
import numpy
import xml.etree.ElementTree as ET
import pprint
import time
import progressbar
from tqdm import tqdm
import re
import multiprocessing
from multiprocessing import Pool
from rti_record_analyzer import sanatize_db


class MyProgressBar(progressbar.ProgressBar):
    """
    Custom Progress bar that overrides the _del_ function
    """
    def __del__(self):
        pass


class MyTqdm(tqdm):
    """
    Custom Progress bar that overrides the _del_ function
    """
    def __del__(self):
        pass


class Tree:

    """Summary
    Empty Tree Node
    """
    pass


class RtiRecordParser():

    """Summary
    
    Attributes:
        data_tree (TYPE): Description
        db_connection (TYPE): Description
        db_cursor (TYPE): Description
        dbfile (TYPE): Description
        kColumnNameIndex (int): Description
        kColumnTypeIndex (int): Description
        kSqlToNumpyType (TYPE): Description
        recorder_config (TYPE): Description
        tables (TYPE): Description
        topics (TYPE): Description
    """
    
    class Table:

        """Summary
        Attributes:
            name (Sting): Name associated with the Node
        """
        
        def __init__(self, name):
            """Summary
            Constructor
            Args:
                name (Sting): Name associated with the Node
            """
            self.name = name

        def types(self):
            """Summary
            Returns all children types if any
            Returns:
                list: List of all child types
            """
            members = vars(self)
            ret_list = list()
            for member in members:
                if member != 'name':
                    ret_list.append(member)
            return ret_list

    class Type:

        """Summary
        
        Attributes:
            name (string): Type Name
            topic (string): Type Associated Topic
        """
        
        def __init__(self, name, topic):
            """Summary
            
            Args:
                name (string): Type Name
                topic (string): Type Associated Topic
            """
            self.name = name
            self.topic = topic

    kColumnNameIndex = 1
    kColumnTypeIndex = 2

    kSqlToNumpyType = {
        'INTEGER': 'int64',
        'INT': 'int64',
        'REAL': 'float32'
    }

    def __init__(self, dbfile, recorder_config):
        """Summary
        
        Args:
            dbfile (string): Path of the Deserialized Sqlite RtiRecord db
            recorder_config (string): Path the Recording service xml
        """
        self.dbfile = dbfile
        try:
            db_connection = sqlite.connect(self.dbfile)
            db_cursor = db_connection.cursor()
        except Exception as e:
            print(" Exception: " + e)
            sys.exit(1)
        self.recorder_config = recorder_config
        self.tables = list()
        self.topics = list()
        self.data_tree = Tree()
        self._data = dict()
        self._get_recorded_types()
        self._check_data()

    def _get_topics_from_db(self, columnregex):
        """Summary
        Parse and extract all relevant Topics from the db
        Args:
            columnregex (String): Column Regular Expression string
        """
        db_connection = sqlite.connect(self.dbfile)
        db_cursor = db_connection.cursor()
        columnregex = '\$'.join(columnregex.split('$'))
        db_cursor.execute('SELECT name FROM sqlite_master WHERE type=\'table\'')
        table_names = db_cursor.fetchall()
        topic_match = re.compile('[A-Za-z0-9]' + columnregex)
        for table in table_names:
            if(topic_match.match(table[0])):
                self.tables.append(table[0])
                temp_topic = table[0].split('$')[0]
                if(temp_topic in self.topics):
                    self.topics.append(temp_topic +
                                       '_' + table[0].split('$')[1])
                else:
                    self.topics.append(temp_topic)


    def _get_recorded_types(self):
        """Summary
        
        """
        tree = ET.parse(self.recorder_config)
        root = tree.getroot()
        topics = dict()
        for topic_group in root.findall("./recorder/topic_group"):
            topics[topic_group.get('name')] = \
                topic_group.find('./topics')[0].text.strip().split(',')
        for record_group in root.findall("./recorder/record_group"):
            for domain in record_group.findall("./domain_ref/*"):
                for topic_group in record_group.findall("./topic_ref/*"):
                    for topic in topics[topic_group.text.strip()]:
                        if topic == '*':
                            self._get_topics_from_db("{}${}${}".format(topic, record_group.get('name'), domain.text.strip()))
                        else:
                            self.tables.append("{}${}${}".format(topic, record_group.get('name'), domain.text.strip()).lower())
                            self.topics.append(topic)

    def _check_data(self):
        """Summary
        """
        db_connection = sqlite.connect(self.dbfile)
        db_cursor = db_connection.cursor()
        existing_tables = list()
        existing_topics = list()
        for table, topic in zip(self.tables, self.topics):
            db_cursor.execute('PRAGMA table_info({})'.format(table))
            table_info = db_cursor.fetchall()
            if len(table_info) != 0:
                existing_topics.append(topic)
                existing_tables.append(table)
        self.tables = existing_tables
        self.topics = existing_topics

    def _fill_data(self, args):
        """Summary
        
        Raises:
            RuntimeError: Description
        """
        (table, topic) = args
        connection = sqlite.connect(self.dbfile)
        cursor = connection.cursor()
        cursor.execute('PRAGMA table_info({})'.format(table))
        table_info = cursor.fetchall()
        if len(table_info) == 0:
            raise RuntimeError('Run Check _check_data() before this')
        _data = dict()
        _data[topic] = dict()
        if('instance' in [tup[self.kColumnNameIndex] for tup in table_info]):
            _data.pop(topic, None)
            cursor.execute('SELECT DISTINCT instance FROM {}'.
                                   format(table), )
            instances = cursor.fetchall()
            for inst in instances:
                _data[topic + '_' + str(inst[0])] = dict()
                for info in table_info:
                    sql_type = info[self.kColumnTypeIndex]
                    if sql_type not in self.kSqlToNumpyType.keys():
                        continue
                    cursor.execute('SELECT \"{}\" FROM {} WHERE instance = {}'.
                                           format(info[self.kColumnNameIndex],
                                                  table, inst[0]), )
                    col = re.sub('\W+', '', info[self.kColumnNameIndex])
                    _data[topic + '_' + str(inst[0])][col] = \
                        numpy.array([tup[0] for tup in cursor.fetchall()
                                     if tup[0] is not None],
                                    dtype=self.kSqlToNumpyType[sql_type])
        elif ('location' in [tup[self.kColumnNameIndex] for tup in table_info]):
            _data.pop(topic, None)
            cursor.execute('SELECT DISTINCT location FROM {}'.
                           format(table), )
            instances = cursor.fetchall()
            for inst in instances:
                _data[topic + '_' + str(inst[0])] = dict()
                for info in table_info:
                    sql_type = info[self.kColumnTypeIndex]
                    if sql_type not in self.kSqlToNumpyType.keys():
                        continue
                    cursor.execute('SELECT \"{}\" FROM {} WHERE location = {}'.
                                   format(info[self.kColumnNameIndex],
                                          table, inst[0]), )
                    col = re.sub('\W+', '', info[self.kColumnNameIndex])
                    _data[topic + '_' + str(inst[0])][col] = \
                        numpy.array([tup[0] for tup in cursor.fetchall()
                                     if tup[0] is not None],
                                    dtype=self.kSqlToNumpyType[sql_type])
        else:
            for info in table_info:
                sql_type = info[self.kColumnTypeIndex]
                if sql_type not in self.kSqlToNumpyType.keys() :
                    continue
                cursor.execute('SELECT \"{}\" FROM {}'.
                                       format(info[self.kColumnNameIndex],
                                              table))
                _data[topic][re.sub('\W+', '',
                                         info[self.kColumnNameIndex])] = \
                    numpy.array([tup[0] for tup in cursor.fetchall()
                                 if tup[0] is not None],
                                dtype=self.kSqlToNumpyType[sql_type])


        return _data

    def _construct_type_tree(self):
        """Summary
        """
        for topic in self._data:
            setattr(self.data_tree, topic, self.Table(topic))
            for col in self._data[topic]:
                setattr(getattr(self.data_tree, topic), col, self.Type(col, topic))

    def GetDataSet(self, type, name=None):
        """Summary
        
        Args:
            type (TYPE): Description
            name (None, optional): Description
        
        Returns:
            TYPE: Description
        """
        try:
            return self._data[type.topic][type.name]
        except AttributeError:
            return self._data[type][name]
        except Exception as e:
            print("Exception " + e)
            return None

    def Init(self, no_cores=0, index=0):
        """Summary
        """

        argslist = [(table, topic) for table, topic in zip(self.tables, self.topics)]
        progress_bar = MyTqdm(total=len(argslist), position=index)
        progress_bar.set_description(desc=sanatize_db(self.dbfile) + "_" + str(index))
        prev_count =0
        if no_cores == 0:
            pool = Pool(maxtasksperchild=4)
        else:
            pool = Pool(processes=no_cores, maxtasksperchild=1)
        thread_results = pool.map_async(self._fill_data, argslist)
        #print("Parsing " + self.dbfile)
        while not thread_results.ready():
            count = len(argslist) - thread_results._number_left
            if count > prev_count:
                progress_bar.update(count-prev_count)
                prev_count = count
            time.sleep(0.2)
        results = thread_results.get()
        progress_bar.update(1)
        progress_bar.close()
        pool.close()
        pool.terminate()
        pool.join()
        for item in results:
            self._data = {**self._data, **item}

    def GetTypeTree(self):
        """Summary
        
        Returns:
            TYPE: Description
        """
        self._construct_type_tree()
        return self.data_tree

    def PrintTypeTree(self):
        """Summary
        """
        self._construct_type_tree()
        for topic in self.topics:
            t = getattr(self.data_tree, topic)
            print(t.name)
            for col in t.types():
                print('\t' + col)

    def PrintData(self):
        """Summary
        """
        pretty_printer = pprint.PrettyPrinter()
        pretty_printer.pprint(self._data)


def main():
    """Summary
    """
    rti_record_parser = RtiRecordParser(sys.argv[1], sys.argv[2])
    rti_record_parser.Init()
    rti_record_parser.PrintTypeTree()

if __name__ == '__main__':
    main()
