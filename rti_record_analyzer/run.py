#!/usr/bin/env python3
"""Summary
"""
import os
import gc
import copy
import pickle
from argparse import ArgumentParser
import queue
import zlib
import multiprocessing.pool
from multiprocessing.pool import worker, util
from rti_record_parser import RtiRecordParser, Tree
from rti_record_plotter import Plotter, PlotterEvents
from rti_record_analyzer import RtiRecordAnalyzer, PandaSeriesOperations, sanatize_db

list_of_files=0
files_at_a_time = 4


class MyPool(multiprocessing.pool.Pool):
    def _repopulate_pool(self):
        """Bring the number of pool processes up to the specified number,
        for use after reaping workers which have exited.
        """
        for i in range(self._processes - len(self._pool)):
            w = self.Process(target=worker,
                             args=(self._inqueue, self._outqueue,
                                   self._initializer,
                                   self._initargs, self._maxtasksperchild,
                                   self._wrap_exception)
                            )
            self._pool.append(w)
            w.name = w.name.replace('Process', 'PoolWorker')
            w.daemon = False
            w.start()
            util.debug('added worker')


def retrieve_cached(name):
    """Summary
    
    Returns:
        TYPE: Description
    """
    with open(name + '/type_tree', 'rb') as f:
        print("Loading Type Tree..")
        type_tree = pickle.loads(zlib.decompress(f.read()))
    with open(name + '/parser', 'rb') as f:
        print("Loading Parser..")
        parser = pickle.loads(zlib.decompress(f.read()))
    return parser, type_tree


def cache(name, parser, type_tree):
    """Summary
    
    Args:
        analyzer (TYPE): Description
        parser (TYPE): Description
        type_tree (TYPE): Description
        db (TYPE): Description
        xml (TYPE): Description
    """
    global list_of_files
    for i in range(list_of_files):
        print('')
    print("\nCaching....")
    gc.disable()
    try:
        os.mkdir(name)
    except FileExistsError:
        pass
    with open(name + '/type_tree', 'wb') as f:
        f.write(zlib.compress(pickle.dumps(type_tree)))
    with open(name + '/parser', 'wb') as f:
        f.write(zlib.compress(pickle.dumps(parser)))
    gc.enable()


def option_handler():
    """Summary
    Argument/option handler
    
    Returns:
        TYPE: Description
    """
    global list_of_files
    parser = ArgumentParser()
    parser.add_argument("-d", "--database", dest="db", nargs="+",
                        help="The SQlite Database from RTI_Recorder [Can have multiple entries]")
    parser.add_argument("-x", "--xml", dest="xml",
                        help="The Configuration xml for RTI Recorder")
    parser.add_argument("-n", "--name", dest="name", default=".cache",
                        help="The Recording name")
    parser.add_argument("-p", "--plotter", dest="plotter",
                        help="Plotter On/Off", default="On")
    parser.add_argument("-P", "--plotPath", dest="path",
                        help="Path to save the Plots", default=".")
    parser.add_argument("-c", "--cores", dest="cores",
                        help="Number of Cores", default=4, type=int)
    options = parser.parse_args()
    if options.db is not None:
        list_of_files = len(options.db)
    return options

def create_series(args):
    (xml, cores, db, index) = args
    temp_parser = RtiRecordParser(db, xml)
    temp_parser.Init(cores, index)
    temp_type_tree = temp_parser.GetTypeTree()
    return temp_parser, temp_type_tree


def process_logs(options):
    global files_at_a_time
    options.cores = int(options.cores/files_at_a_time)
    arglist = [(options.xml, options.cores, db, index) for index, db in enumerate(options.db)]
    pool = MyPool(processes=files_at_a_time, maxtasksperchild=1)
    thread_results = pool.map(create_series, arglist)
    pool.close()
    pool.join()
    parser = Tree()
    type_tree = Tree()
    for i in range(len(thread_results)):
        db = options.db[i]
        temp_parser = copy.deepcopy(thread_results[i][0])
        temp_type_tree = copy.deepcopy(thread_results[i][1])
        db = sanatize_db(db)
        setattr(parser, db, temp_parser)
        setattr(type_tree, db, temp_type_tree)
    thread_results.clear()
    return parser, type_tree


def run_interactive(options, series_queue, event_handler = None):
    """Summary
    
    Args:
        options (TYPE): Description
        event_handler (TYPE): Description
        series_queue (TYPE): Description
    """
    if(options.db):
        try:
            parser, type_tree = retrieve_cached(options.name)
            print("Retrieved Cached Database for " + options.name)
        except FileNotFoundError:
            (parser, type_tree) = process_logs(options)
            cache(options.name, parser, type_tree)
    else:
        try:
            parser, type_tree = retrieve_cached(options.name)
            print("Retrieved Cached Database for " + options.name)
        except FileNotFoundError:
            print("Please Provide a Database and Recorder.xml file. Use -h for Help")
            print("Press Ctrl + C to exit")
            exit()
    if options.plotter == 'On':
        series = PandaSeriesOperations(type_tree, parser, options.db)
        analyzer = RtiRecordAnalyzer(series, event_handler, series_queue, options.path)
        import IPython
        IPython.embed(
            banner2="Variables available: \n\t1.parser.database  - access Database elements\n\t2.analyzer - Analyze Data Sets of valid types\n\t3.type_tree.database - available data types to analyze\n",
            exit_msg="Press Ctrl + C to exit")



def main():
    """Summary
    """
    options = option_handler()
    series_queue = queue.Queue()
    if options.plotter == "On":
        import threading
        plotter = Plotter(series_queue, options.path)
        ipython = threading.Thread(target=run_interactive,
                                    args=(options,
                                     series_queue,
                                     plotter.GetEventHandler()))
        ipython.start()
        plotter.Run()
    else:
        run_interactive(options, series_queue)


if __name__ == '__main__':
    main()