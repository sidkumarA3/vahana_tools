"""Summary
"""
import os
import datetime
import time
import threading
import pathlib
from matplotlib import pyplot
from matplotlib import dates
import numpy as np


class PlotterEvents(object):
    """docstring for PlotterEvents
    
    Attributes:
        clear (threading.Event):  Event flag to trigger a clear/erase of the plot
        draw_plot (threading.Event): Event flag to trigger a drawing of the plot
        save (threading.Event): Event flag to save the plot
    """
    def __init__(self):
        """Summary
        Constructor
        """
        # Synchronization Primitives
        self.clear = threading.Event()
        self.clear.clear()
        self.save = threading.Event()
        self.save.clear()
        self.draw_plot = threading.Event()
        self.draw_plot.clear()
        self.add = threading.Event()
        self.add.clear()
        self.new = threading.Event()
        self.new.clear()

    def Draw(self, obj):
        """Summary
        Render/Refersh the plot
        Args:
            obj (Plotter): Object that implements the __draw__() function
        """
        if self.draw_plot.is_set():
            obj.__draw__()
            self.draw_plot.clear()

    def Clear(self, obj):
        """Summary
        Clear the plot
        Args:
           obj (Plotter): Object that implements the __clear__() function
        """
        if self.clear.is_set():
            obj.__clear__()
            self.clear.clear()

    def Add(self, obj):
        """Summary
        Add a subplot
        Args:
           obj (Plotter): Object that implements the __clear__() function
        """
        if self.add.is_set():
            obj.__add__()
            self.add.clear()

    def Save(self, obj):
        """Summary
        Save the plotted figure
        Args:
            obj (Plotter): Object that implements the __save__() function
        """
        if self.save.is_set():
            obj.__save__()
            self.save.clear()

    def New(self, obj):
        """Summary
        Create New Figure
        Args:
            obj (Plotter): Object that implements the __save__() function
        """
        if self.new.is_set():
            obj.__newfig__()
            self.new.clear()

    def SetDraw(self):
        """Summary
        Set the Draw event
        """
        self.draw_plot.set()

    def SetClear(self):
        """Summary
        Set the Clear event
        """
        self.clear.set()

    def SetSave(self):
        """Summary
        Set the Save event
        """
        self.save.set()

    def SetAdd(self):
        """Summary
        Set the Add event
        """
        self.add.set()

    def SetNew(self):
        """Summary
        Set the Add event
        """
        self.new.set()


class Plotter(object):
    """docstring for Plotter
    
    Attributes:
        curr_plot (list): List of all current plots
        events (PlotterEvents): Object that triggers events
        figure (pyplot.figure): Object for the pyplot figure
        plot (pyplot.figure.subplot): Current Plot
        queue (threading.Queue): Queue to share the panda series to be plotted
    """
    def __init__(self, queue, path="."):
        """Summary
        Constructor
        Args:
             queue (threading.Queue): Queue to share the panda series to be plotted
        """
        super(Plotter, self).__init__()
        self.queue = queue
        self.path = path + "/plots/"
        self.figure = pyplot.figure()
        self.plot = None #self.figure.add_subplot(111)
        self.curr_plot = list()
        self.annotate_plot = 0
        self.annotations = list()
        pyplot.ion()
        self.events = PlotterEvents()
        cid = self.figure.canvas.mpl_connect('button_press_event', self.onclick)
        cid = self.figure.canvas.mpl_connect('key_press_event', self.onkeypress)

    def __draw__(self):
        """Summary
        Function to draw/refresh the plot
        """
        series = self.queue.get()
        if series.name.find('Vs') == -1:
            if self.plot is None:
                self.plot = self.figure.add_subplot(111)
            self.curr_plot.append(self.plot.plot(series,
                                                 label=series.name,
                                                 marker='o',
                                                 markersize=2,
                                                 linestyle='dashed',
                                                 linewidth=1))
        else:
            if self.plot is None:
                if series.name.find('heading') != -1:
                    self.plot = self.figure.add_subplot(111, projection='polar')
                    if series.name.find('signal') != -1:
                        self.plot.set_rticks([-80, -85, -95, -105,-250])
                else:
                    self.plot = self.figure.add_subplot(111)
            self.curr_plot.append(self.plot.plot(list(series.index),
                                                    list(series.data),
                                                    label=series.name,
                                                    marker='o',
                                                    linewidth=0))
        mng = pyplot.get_current_fig_manager()
        mng.resize(1900, 1000)
        if series.name.find('Vs') == -1:
            self.plot.xaxis.set_major_formatter(dates.DateFormatter('%d %H:%M:%S.%f'))
        self.plot.legend()
        self.plot.relim()
        self.plot.autoscale_view(True, True, True)
        self.plot.grid('on')
        pyplot.show()

    def onclick(self, event):
        if event.dblclick:
            x_array = np.array([((np.datetime64(t).astype('int64'))/1e9) for t in self.curr_plot[self.annotate_plot][0].get_data()[0]])
            x_data = np.asarray(dates.num2epoch(event.xdata))
            closest_x_idx = np.abs(x_array - x_data).argmin()
            value_x = (np.datetime64(self.curr_plot[self.annotate_plot][0].get_data()[0][closest_x_idx]).astype('int64'))/1e9
            value_y = self.curr_plot[self.annotate_plot][0].get_data()[1][closest_x_idx]
            time_stamp = datetime.datetime.utcfromtimestamp(value_x).strftime("%H:%M:%S.%f")
            self.annotations.append(self.plot.annotate("{}: {}".format(time_stamp, value_y), xy=(dates.epoch2num(value_x), value_y), xytext=(-15, 15), xycoords = 'data', textcoords='offset points', picker=True,
                                    bbox=dict(boxstyle='round,pad=.5', fc='yellow', alpha=.5, edgecolor='black'),
                                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3', shrinkB=0,edgecolor='black'), fontSize=10
                                    ))
            self.figure.canvas.draw()
            pyplot.show()

    def onkeypress(self, event):
        if event.key == 'C':
            print("Clearing Annotations")
            for ann in self.annotations:
                print(ann)
                ann.remove()
            self.annotations = list()
            self.figure.canvas.draw()
        if event.key == '!':
            print("Annotate Plot 1")
            self.annotate_plot = 0
        if event.key == '@':
            print("Annotate Plot 2")
            self.annotate_plot = 1
        if event.key == '#':
            print("Annotate Plot 3")
            self.annotate_plot = 2
        if event.key == '$':
            print("Annotate Plot 4")
            self.annotate_plot = 3
        if event.key == '%':
            print("Annotate Plot 5")
            self.annotate_plot = 4
        if event.key == 'A':
            print("AutoScale")
            mng = pyplot.get_current_fig_manager()
            mng.resize(1900, 1000)
            self.plot.legend()
            self.plot.relim()
            self.plot.autoscale_view(True, True, True)
            self.plot.grid('on')
            pyplot.show()



    def __add__(self):
        """Summary
            Function to Add a subplot
        """
        self.plot.change_geometry(2, 1, 1)
        ax1 = self.plot
        self.plot = self.figure.add_subplot(2,1,2, sharex=ax1)
        self.figure.canvas.draw()

    def __clear__(self):
        """Summary
        Function to clear the plot 
        """
        for plot in self.curr_plot:
            plot[0].remove()
        self.curr_plot = list()
        for ann in self.annotations:
            ann.remove()
        self.annotations = list()
        pyplot.clf()
        self.plot = None
        cid = self.figure.canvas.mpl_connect('button_press_event', self.onclick)
        cid = self.figure.canvas.mpl_connect('key_press_event', self.onkeypress)
        self.figure.canvas.draw()

    def __newfig__(self):
        """Summary
        Function to Create a new Figure
        """
        self.curr_plot = list()
        self.annotations = list()
        self.figure = pyplot.figure()
        pyplot.clf()
        self.plot = None
        cid = self.figure.canvas.mpl_connect('button_press_event', self.onclick)
        cid = self.figure.canvas.mpl_connect('key_press_event', self.onkeypress)
        self.figure.canvas.draw()

    def __save__(self):
        """Summary
        Function to save the plot
        """
        file_name = self.queue.get() +\
            datetime.datetime.now().strftime("%Y-%m-%d-%H:%m" + '.png')
        pathlib.Path(self.path).mkdir(parents=True, exist_ok=True)
        self.figure.savefig(self.path + "/" + file_name)
        print("Saved: " + self.path + "/" + file_name )

    def GetEventHandler(self):
        """Summary
        
        Returns:
            PlotterEvents: Event Handler
        """
        return self.events

    def Run(self):
        """Summary
        Main Loop Refreshes plot and handles any events that our triggered
        via the user
        Returns:
            None
        """
        try:
            while True:
                time.sleep(0.1)
                if pyplot.fignum_exists(self.figure.number):
                    self.figure.canvas.flush_events()
                else:
                    self.figure = pyplot.figure()
                    self.plot = None
                self.events.Draw(self)
                self.events.Clear(self)
                self.events.Save(self)
                self.events.Add(self)
                self.events.New(self)
        except KeyboardInterrupt:
            return
